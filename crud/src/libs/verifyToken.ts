import { Request, Response, NextFunction} from 'express';
import jwt from 'jsonwebtoken';
interface IPayload {
    _id: string;
    iat: number;
}
let coordToGetCases: string;
/* Agarra el token mandado desde token-interceptor.service.ts y valida a que usuario pertenece.
Si el usuario existe agrega objetos al Request y continua en coordController.ts */
export const TokenValidation = (req: Request, res: Response, 
    next: NextFunction) => {
    if (!req.headers.authorization) {
        return res.status(401).send('No estas autorizado');
    }
    const token = req.headers.authorization.split(' ')[1];
    if(token === 'null') {
        return res.status(401).send('No estas autorizado');
    }
    jwt.verify(token, 'secretKey', (err, decoded: any) => {
        if ( err ) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'Token no valido'
                }
            });
        }
        req.coordEmail = decoded.email; 
        req.coordRole = decoded.coordRole;
        next();
    });
}