import { Request, Response } from 'express';
import pool from '../database';
import maily from '../email';
import CryptoJS from 'crypto-js';

interface CaseToGo{
    case_folio: string;
    alum_id: number;
    alum_name: string;
    alum_email:string;
    coord_name:string;
    cord_id:number;
    q_cont:string;
    q_obs:string;
    a_cont:string;
    a_obs:string;
    case_pro:string;
    case_status:boolean;
    case_date:Date;
}

interface CaseToStore{
    case_folio: string;
    alum_id: number;
    alum_name: string;
    alum_email:string;
    coord_name:string;
    cord_id:number;
    q_cont:string;
    q_obs:string;

}


class StudentController {
    public async getQuerys(req: Request, res: Response): Promise<void> {
        const games = await pool.query('SELECT * FROM query');
        let querys = games as string[];
        res.json(querys);
    }

    public async getCareers(req: Request, res: Response): Promise<void> {
        const games = await pool.query('SELECT * FROM career');
        let querys = games as string[];
        res.json(querys);
    }

    public async getStudent(req: Request, res: Response): Promise<void> {
        let body = req.body;
        const passphrase = "z&.GuFq)escM7'=+";
        const bytes = CryptoJS.AES.decrypt(body.student_pass, passphrase);
        const originalText = bytes.toString(CryptoJS.enc.Utf8);

        const student = await pool.query('SELECT * FROM alumno WHERE alum_id = ? AND alum_nip = ?',
            [body.student_id, originalText]).catch(error => {res.status(400).json({ message: 'error'});
        return;
        });
        var stu = {alum_name:'', alum_email:''}
        // console.log(student[0]['alum_name'])
        // console.log(student[0]['alum_email'])
        if (student[0] != null) {
          stu.alum_name = student[0]['alum_name']
          stu.alum_email = student[0]['alum_email']
        }
        res.json(stu);
    }

    public async getMats(req: Request, res: Response): Promise<void> {
        let query = "SELECT * FROM materia WHERE";
        let body = req.body;
        // console.log(body);
        let NoField = true;
        let OneField = false;
        if ( ( !isNaN(body.career) && !isNaN(parseFloat(body.career)) ) || body.career == "0" ){
            query+= " car_id = "+body.career;
            NoField = false
            OneField = true;
        }
        if ( ( !isNaN(body.semester) && !isNaN(parseFloat(body.semester)) ) || body.semester == "0"){
            if (OneField) {query+= " AND";}
            query+= " mat_sem = "+body.semester;
            NoField = false
            OneField = true;
            console.log(query);
        }
        if (NoField){query+= " 1";}
            const games = await pool.query(query).catch(error => {
            console.log(error);
            res.status(400).json({ message: 'error getting subjects'});
            return;
        });
        let mats = games as string[];
        res.json(mats);
    }
    // Generar nuevo caso
    public async generateCase(req: Request, res: Response): Promise<any> {
        const date = new Date();
        //Verificar el estudiante
        const cord = await pool.query('SELECT career_sig FROM career WHERE career_id = ? ',
            [req.body.coord_name]).catch(error => {res.status(400).json({ message: 'error'});
        return;
        });
        const Ver = await pool.query('SELECT * FROM alumno WHERE alum_id = ? AND alum_name = ? AND alum_email = ? AND cord_id = ?',
            [req.body.alum_id,req.body.alum_name,req.body.alum_email,req.body.coord_name]).catch(error => {
                res.status(400).json({ message: 'error'});
            return;
        });
        if (Ver.length > 0){
            var alumdate = Ver[0].alum_matdate;

            // Comprobación para la modificación de horario dependiendo de la hora de seleccion de materias
            // INICIO

            // console.log('alumdate  '+alumdate+ ' vs. dateNow '+date);
            // console.log(date < alumdate);
            // console.log(req.body.q_cont === "MODIFICACION DE HORARIO" );

            // if (date < alumdate && req.body.q_cont === "MODIFICACION DE HORARIO"){
            //     console.log(alumdate);
            //     console.log(date);
            //     return res.status(400).json({ message: 'Fuera de la hora de seleccion' });
            // } else {
            //     //Generar folio
            //         var newFolio: string;
            //         var aux : string;
            //         newFolio=date.getFullYear()+"";
            //         newFolio=newFolio + (date.getMonth() > 7 ? "3": date.getMonth() > 5 ? "2": "1");
            //         newFolio= newFolio+ req.body.alum_id;
            //         aux=req.body.coord_name+"";
            //         newFolio=  newFolio+ aux.padStart(2,"0");
            //         const casesCont = await pool.query('SELECT * FROM casos WHERE case_folio LIKE ?', [newFolio+'%']).catch(error => {
            //                 res.status(400).json({ message: 'error'});
            //                 return;
            //             });
            //         aux = (casesCont.length+1).toString();
            //         casesCont.forEach(function(val: any) {
            //             if(val.case_folio <= newFolio+ aux.padStart(3,"0")){
            //                     aux= ( parseInt( val.case_folio.slice(-3) ) +1 ).toString();
            //             }
            //             });
            //         newFolio=  newFolio+ aux.padStart(3,"0");
            //         //Enviar datos
            //         let ToStore = req.body as CaseToStore;
            //         ToStore.case_folio=newFolio;
            //         ToStore.cord_id=req.body.coord_name;
            //         ToStore.coord_name=cord[0].career_sig;
            //         const result = await pool.query('INSERT INTO casos set ?', [ToStore]).then(mensaje => {
            //             maily.NewMail(ToStore.alum_email,newFolio);
            //                 res.status(200).json({ message: 'Success', Folio: newFolio });
            //             })
            //             .catch(error => {
            //                 console.log(error);
            //                 res.status(400).json({ message: 'Error saving case'});
            //             });
            // }

            // FIN
            var newFolio: string;
            var aux : string;
            newFolio=date.getFullYear()+"";
            newFolio=newFolio + (date.getMonth() > 7 ? "3": date.getMonth() > 5 ? "2": "1");
            newFolio= newFolio+ req.body.alum_id;
            aux=req.body.coord_name+"";
            newFolio=  newFolio+ aux.padStart(2,"0");
            const casesCont = await pool.query('SELECT * FROM casos WHERE case_folio LIKE ?', [newFolio+'%']).catch(error => {
                    res.status(400).json({ message: 'error'});
                    return;
                });
            aux = (casesCont.length+1).toString();
            casesCont.forEach(function(val: any) {
                if(val.case_folio <= newFolio+ aux.padStart(3,"0")){
                        aux= ( parseInt( val.case_folio.slice(-3) ) +1 ).toString();
                }
                });
            newFolio=  newFolio+ aux.padStart(3,"0");
            //Enviar datos
            let ToStore = req.body as CaseToStore;
            ToStore.case_folio=newFolio;
            ToStore.cord_id=req.body.coord_name;
            ToStore.coord_name=cord[0].career_sig;
            const result = await pool.query('INSERT INTO casos set ?', [ToStore]).then(mensaje => {
                maily.NewMail(ToStore.alum_email,newFolio);
                    res.status(200).json({ message: 'Success', Folio: newFolio });
                })
                .catch(error => {
                    console.log(error);
                    res.status(400).json({ message: 'Error saving case'});
                });
        } else {
            console.log(Ver);
            return res.status(400).json({ message: 'Alumno no reconocido' });
    }}

    public async consultCase(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const cases = await pool.query('SELECT * FROM casos WHERE case_folio = ?', [id]).catch(error => {
            res.status(400).json({ message: 'error getting case'});
            return;
        });
        if (cases.length > 0) {
            let ToSend :CaseToGo={
                case_folio: cases[0].case_folio,
                alum_id: cases[0].alum_id,
                alum_name: cases[0].alum_name,
                alum_email:cases[0].alum_email,
                coord_name:cases[0].coord_name,
                cord_id:cases[0].cord_id,
                q_cont:cases[0].q_cont,
                q_obs:cases[0].q_obs ? cases[0].q_obs : "",
                a_cont: cases[0].a_cont ? cases[0].a_cont : "",
                a_obs:cases[0].a_obs ? cases[0].a_obs : "",
                case_pro:cases[0].case_pro,
                case_status:cases[0].case_status.readUIntLE(0,1) ? true : false,
                case_date:new Date(cases[0].case_date),
            };
        return res.json(ToSend);
        }
        res.status(404).json({ text: "The Case doesn't exits" });
    }
}
const studentController = new StudentController;
export default studentController;