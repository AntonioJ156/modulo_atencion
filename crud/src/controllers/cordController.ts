import { Request, Response, NextFunction, json } from 'express';
import { ICase,OCase } from "../models/Case";
import { ICoord } from "../models/Coord";
import jwt from "jsonwebtoken";
import pool from '../database';
import maily from '../email';
import bcrypt from "bcrypt";
class CordController {
    // Registrar un nuevo coordinador
    // No subido para una nueva tabla
    public async registerNewCoord(req: Request, res: Response): Promise<void> {
        const cordIDs = await pool.query('SELECT cord_id FROM cord');
        var newID:number = 0;
        if (cordIDs.length > 0) {
            newID=1;
            cordIDs.forEach((cid: any) => {
                if (cid.cord_id == newID){
                    newID++;
                }
            });
        }
        let body = req.body;
        let newCoord = {
            ...(body.cord_id || body.cord_id == undefined) && { cord_id:newID },
            ...(body.cord_name || body.cord_name == undefined) && { cord_name: body.cord_name},
            ...(body.cord_name || body.cord_name == undefined) && { cord_email: body.cord_email},
            ...(body.cord_pass) && { cord_pass:bcrypt.hashSync(body.cord_pass, 10) }
        }
        await pool.query('INSERT INTO cord set ?',[newCoord]).then( async (CareerAns:any) =>  {
            const cord = await pool.query('SELECT cord_id FROM cord WHERE cord_email = ? ', [body.cord_email]).catch(error => {
                return;
            });
            if (body.cord_crs.length > 0){
                var CordCareers : any [] = body.cord_crs.split(",");
                var Relations :any[] = [];
                CordCareers.forEach(car => {
                    var num = car;
                    Relations.push( [ parseInt(num) , parseInt(cord[0].cord_id) ] );
                });
                await pool.query("INSERT INTO car_cord(car_id,cord_id) VALUES ?",[Relations]).catch((error: any) => {
                    console.log(error);
                    return;
                });
            }
        }, (error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error saving User'});
        } );
        const token = jwt.sign({_id: newCoord.cord_email},'secretKey');
        res.status(200).json({newID});
    };
    //Subida a la tabla de coordinadores
    public async updateCoord(req: Request, res: Response): Promise<void> {
        let body = req.body;
        // Se eliminan todas las entradas en la tabla
        await pool.query("DELETE FROM car_cord WHERE cord_id = ?",[body.cord_id]).catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error deleting permits'});
        });
        let Coord = {
            cord_name: body.cord_name,
            cord_email: body.cord_email,
            ...(body.cord_pass) && { cord_pass:bcrypt.hashSync(body.cord_pass, 10) }
        }
        var CordCareers : any [] = body.cord_crs.split(",");
        var Relations :any[] = [];
        CordCareers.forEach(car => {
            var num = car;
            Relations.push( [ parseInt(num) , parseInt(body.cord_id) ] );
            });
        await pool.query('UPDATE `cord` SET ? WHERE `cord`.`cord_id` = ?',[Coord,body.cord_id]).catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error saving User'});
        } );
        await pool.query("INSERT INTO car_cord(car_id,cord_id) VALUES ?",[Relations]).catch((error: any) => {
                console.log(error);
                res.status(400).json({ message: 'Error updating user permits'});
            });
            res.status(200).json("User updated");
    }
    public async deleteCoord(req: Request, res: Response): Promise<void> {
        let body = req.body;
        // Se borran los datos en la tabla
        await pool.query("DELETE FROM car_cord WHERE cord_id = ?",[body.cord_id]).catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error deleting permits'});
        });

        await pool.query("DELETE FROM cord WHERE cord_id = ?",[body.cord_id]).catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error dleting user'});
        });
        res.status(200).json("User deleted");
    }
    // Inicio de sesión de la coordinadora
    public async signinCord(req: Request, res: Response): Promise<any> {
        const cordEmail = req.body.cord_email;
        const cordPass = req.body.cord_pass;
        console.log(cordEmail);
        console.log(cordPass);
        const emailFound = await pool.query('SELECT cord_id FROM cord WHERE cord_email = ? ', [cordEmail]);
        if(emailFound.length === 0) return res.status(401).send('El usuario no existe ');
        const passFound = await pool.query('SELECT cord_pass FROM cord WHERE cord_email = ? ', [cordEmail]);
        // Compara la constraseña dada con con la constraseña que hay en la basede datos
        if( !bcrypt.compareSync( cordPass, passFound[0].cord_pass )) return res.status(401).
            send('La contraseña es incorrecta');
        const coordName = await pool.query('SELECT cord_id FROM cord WHERE cord_email = ? ', [cordEmail]);
        // Genera un token donde almacena el nombre del usuario y su rol
        const token = jwt.sign({
                                email: cordEmail,
                                coordRole: coordName[0].cord_id
                                },'secretKey');
        res.status(200).json({ token, coordRole: coordName[0].cord_id });
    }
    // Enviar respuesta
    public async getAnswers(req: Request, res: Response): Promise<void> {
        console.log(req.params.question_id);
        const games = await pool.query('SELECT a.a_cont FROM answer a, query q WHERE q.q_id = ? and a.q_id = ?',[req.params.question_id,req.params.question_id]).catch(error => {
            res.json({ message: 'error'});
            return;
        });
        let querys = games as string[];
        res.json(querys);
    }
    // Tener los nombres del coordinador
    public async getCoordNames(req: Request, res: Response): Promise<void> {
        const games = await pool.query('SELECT * FROM cord');
        const crs = await pool.query('SELECT * FROM car_cord');
        let querys: ICoord [] = [];
        games.forEach( (g: any) => {
            let cordCrs ="";
            crs.forEach( (c: any) => {
                if (c.cord_id === g.cord_id){
                    cordCrs+=c.car_id + ",";
                }
            });
            cordCrs = cordCrs.slice(0, -1);
                querys.push(
                    {
                        cord_id: g.cord_id,
                        cord_name: g.cord_name,
                        cord_pass: g.cord_pass,
                        cord_email: g.cord_email,
                        cord_crs: cordCrs
                    }
                );
        });
        res.json(querys);
    }
    // Get all cases
    /* Utiliza el Request que viene desde verifyToke.ts para hacer la consulta respectiva
    al coordinador */
    public async getAllCases(req: Request, res: Response): Promise<void> {
        let games = [];
        if ( req.coordRole === '99' ) {
            games = await pool.query('SELECT * FROM casos').catch(error => {
                res.json({ message: 'error getting admin'});
                return;
            });
        } else {
            let quer=
            `SELECT b.* FROM cord a, casos b, car_cord c WHERE c.car_id = b.cord_id AND a.cord_id = c.cord_id  AND a.cord_id = (${req.coordRole})`;
            games = await pool.query(quer).catch(error => {
                res.json({ message: 'error loading role'});
                return;
            });
        }
        games.forEach((g: { q_obs: any; a_cont: any; a_obs: any; case_status: any; case_date: any; }) => {
        g.q_obs= g.q_obs ? g.q_obs :"";
        g.a_cont= g.a_cont ? g.a_cont :"";
        g.a_obs= g.a_obs ? g.a_obs :"";
        g.case_status= g.case_status.readUIntLE(0,1) ? true : false;
        g.case_date= new Date(g.case_date);
        });
        let cases: ICase = games;
        res.status(200).json(cases);
    }
    // Subir caso
    public async updateCase(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        console.log(id)
        let ToSend :OCase={
            case_folio:  req.body.case_folio,
            alum_id:     req.body.alum_id,
            alum_name:   req.body.alum_name,
            alum_email:  req.body.alum_email,
            coord_name:  req.body.coord_name,
            cord_id:     req.body.cord_id,
            q_cont:      req.body.q_cont,
            q_obs:       req.body.q_obs ? req.body.q_obs : "",
            a_cont:      req.body.a_cont ? req.body.a_cont : "",
            a_obs:       req.body.a_obs ? req.body.a_obs : "",
            case_pro:    req.body.case_pro ? req.body.case_pro : "N/A",
            case_status: req.body.case_status ? 1 : 0,

        };
        console.log(ToSend)
        await pool.query('UPDATE casos set ? WHERE case_folio = ?', [ToSend, id])
        .then( mensaje => {
           maily.UpMail(req.body.alum_email,req.body.case_folio);
            res.status(200).json({message: 'Case updated'});
        })
        .catch(error => {
            console.log(error);
            res.status(400).json({ message: 'Error updating case'});
        });
    }
}
const cordController = new CordController;
export default cordController;