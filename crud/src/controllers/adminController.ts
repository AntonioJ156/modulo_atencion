import { Request, Response, NextFunction, json } from 'express';
import pool from '../database';
import xlsx from 'node-xlsx';
import fs from 'fs';
// interface de Json
interface QnA{
    id: number;
    question: string;
    answers: Array<string>;

}
class CordController {
    // Obtenga todas las preguntas y respuestas
    public async getQnA (req: Request, res: Response): Promise<void> {
        const quers = await pool.query('SELECT * FROM query');
        const answs = await pool.query('SELECT * FROM answer');
        let qas : QnA[]= [];
        quers.forEach(async (g: any) => {
            qas.push({
                id : g.q_id,
                question : g.q_cont,
                answers : []
            });
            let ans = answs.filter((x: any) => x.q_id == g.q_id);
            ans.forEach(async (a:any) => {
                qas[qas.length-1].answers.push(a.a_cont);
            });
        });
        res.json(qas);
    }
    // Guarde todo el nuevo JSON de preguntas y respuestas
    public async setQnA (req: Request, res: Response): Promise<void> {
        console.log('dentro el update');
        await pool.query("TRUNCATE TABLE query").catch(error => {
            console.log(error);
            res.status(400).json({ message: 'Error truncating Qs table'});
        });
        await pool.query("TRUNCATE TABLE answer").catch(error => {
            console.log(error);
            res.status(400).json({ message: 'Error truncating As table'});
        });
        let qid= 0;
        let nq: any[]  =[];
        let na: any[] =[];
        req.body.forEach((rb:any) => {
            qid++;
            nq.push([qid,rb.question]);
                let aid = 100*qid;
                rb.answers.forEach((rba:any) =>  {
                    aid++;
                    na.push([aid,rba,qid]);
            });
        });
        await pool.query("INSERT INTO answer(a_id,a_cont,q_id) VALUES ?",[na]).catch(error => {
            console.log(error);
            res.status(400).json({ message: 'Error saving answer'});
        });
        await pool.query("INSERT INTO query(q_id,q_cont) VALUES ?",[nq]).catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error saving question'});
        });
        res.json("UPDATE SUCCEFUL");
    }
    // entrada de datos de estudiantes
    public async setStudentDump (req: Request, res: Response): Promise<void> {
        //Borra todos los datos de la tabla
        await pool.query("TRUNCATE TABLE alumno").catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error truncating Students'});
        });
        const stuSheet = xlsx.parse(`./uploads/dump.xlsx`);
        // const stuSheet = xlsx.parse(`/var/www/html/gitRepo/modulo_atencion/crud/uploads/dump.xlsx`);
        let studs:any[] = stuSheet[0].data;
        studs.forEach((st:any[]) => {
            st[1] = st[1].toUpperCase();
            // console.log(st[4]);
            st[4] = new Date(  ((parseFloat(st[4]) - (25569))*86400*1000) - 64800000   )  ;
        });
        await pool.query("INSERT INTO alumno(alum_id,alum_name,alum_email,cord_id,alum_matdate,alum_nip) VALUES ?",
            [studs]).catch((error: any) => {console.log(error);
            res.status(400).json({ message: 'Error saving students'});
        });
        fs.unlinkSync('./uploads/dump.xlsx');
        // fs.unlinkSync('/var/www/html/gitRepo/modulo_atencion/crud/uploads/dump.xlsx');
        res.status(200).json("Ok");
    }
    // Borrar todo el contenido de la tabla alumnos
    public async deleteStudent (req: Request, res: Response): Promise<void> {
        await pool.query("TRUNCATE TABLE alumno").catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error truncating Students'});
        });
        res.status(200).json("Deleted");
    }
    // Borrar todo el contenido de la tabla materia
    public async deleteMaterias (req: Request, res: Response): Promise<void> {
        await pool.query("TRUNCATE TABLE materia").catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error truncating Subjects'});
        });
        res.status(200).json("Deleted");
    }
    // Borrar todo el contenido de la tabla de casos
    public async deleteCasos (req: Request, res: Response): Promise<void> {
        await pool.query("TRUNCATE TABLE casos").catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error truncating Cases'});
        });
        res.status(200).json("Deleted");
    }
    // enviar las materias del excel subido y contenido como dump.xlsx
    public async setMaterias (req: Request, res: Response): Promise<void> {
        const matSheet = xlsx.parse(`./uploads/dump.xlsx`);
        // const matSheet = xlsx.parse(`/var/www/html/gitRepo/modulo_atencion/crud/uploads/dump.xlsx`);
        let mats:any[] = matSheet[0].data;
        await pool.query("INSERT INTO materia(mat_cont,car_id,mat_sem) VALUES ?",[mats]).catch((error: any) => {
            console.log(error);
            res.status(400).json({ message: 'Error saving subjects'});
        });
        fs.unlinkSync('./uploads/dump.xlsx');
        // fs.unlinkSync('/var/www/html/gitRepo/modulo_atencion/crud/uploads/dump.xlsx');
        res.status(200).json("Ok");
    }
}
const cordController = new CordController;
export default cordController;