// Datos generados para tener la relación con la base de datos
export interface ICoord {
    cord_id: number;
    cord_name: string;
    cord_email:string;
    cord_crs: string;
    cord_pass: string;
}