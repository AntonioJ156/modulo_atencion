// Instrucciónes de casos para la relación de datos con la base de datos
export interface ICase {
    case_folio: number;
    alum_id: number;
    alum_name: string;
    alum_email: string;
    coord_name: string;
    cord_id:number;
    q_cout: string;
    q_obs: string;
    a_cout: string;
    a_obs?: string;
    case_pro?: string; 
    case_status?: boolean;
    case_date?:string;
} 
export interface OCase {
    case_folio: number;
    alum_id: number;
    alum_name: string;
    alum_email: string;
    coord_name: string;
    cord_id:number;
    q_cont: string;
    q_obs: string;
    a_cont: string;
    a_obs?: string;
    case_pro?: string; 
    case_status?: number;
    case_date?:Date;
}