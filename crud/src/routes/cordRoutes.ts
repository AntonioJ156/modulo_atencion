/**
 * Instrucciones del rol de coordinadora
 */
import express, { Router } from 'express';
import { TokenValidation } from "../libs/verifyToken";
import cordController from '../controllers/cordController';
class CordRoutes {
    router: Router = Router();
    constructor() {
        this.config();
    }
    config() {
        this.router.post('/register', cordController.registerNewCoord);
        this.router.post('/deletecord', cordController.deleteCoord);
        this.router.post('/updatecord', cordController.updateCoord);
        this.router.post('/signin', cordController.signinCord);
        this.router.get('/cases', TokenValidation, cordController.getAllCases);
        this.router.get('/users', cordController.getCoordNames);
        this.router.get('/answers/:question_id', cordController.getAnswers);
        this.router.put('/update/:id', TokenValidation, cordController.updateCase);
    }
}
export default new CordRoutes().router;