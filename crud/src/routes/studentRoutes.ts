/**
 * Instrucciones del rol de alumno
 */
import express, { Router } from 'express';
import studentController from '../controllers/studentController';
class StudentRoutes {
    router: Router = Router();
    constructor() {
        this.config();
    }
    /**
     * Envio de datos por la red
     */
    config() {
        this.router.post('/m', studentController.getMats);
        // this.router.get('/s/:control/:email', studentController.getStudent);
        this.router.post('/s', studentController.getStudent);
        this.router.get('/q', studentController.getQuerys);
        this.router.get('/crs', studentController.getCareers);
        this.router.get('/:id', studentController.consultCase);
        this.router.post('/', studentController.generateCase);
    }
}
export default new StudentRoutes().router;