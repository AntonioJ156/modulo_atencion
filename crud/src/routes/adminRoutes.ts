/**
 * Instrucciones para el administrador tanto de subida de archivos del administrador
 */

import express, { Router } from 'express';
import { TokenValidation } from "../libs/verifyToken";
import multer from 'multer';
import adminController from '../controllers/adminController';

class AdminRoutes {
    router: Router = Router();
    fileFilter = function (req: any, file: any, cb: any) {
        // accept image only
        if (!file.originalname.match(/\.(xlsx|xls)$/)) {
            return cb(new Error('Only Excel files are allowed!'), false);
        }
        cb(null, true);
    };
    storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, 'uploads/')
          // cb(null, '/var/www/html/gitRepo/modulo_atencion/crud/uploads/')
        },
        filename: function (req, file, cb) {
          cb(null, 'dump.xlsx')
        }
      })
    upload = multer({ storage: this.storage, fileFilter: this.fileFilter });
    constructor() {
        this.config();
    }
    config() {
        this.router.get('/getqna', adminController.getQnA);
        this.router.put('/setqna', adminController.setQnA);
        this.router.delete('/delstu', adminController.deleteStudent);
        this.router.delete('/delmat', adminController.deleteMaterias);
        this.router.delete('/delcase', adminController.deleteCasos);
        this.router.post('/setstu',this.upload.single('test'), adminController.setStudentDump);
        this.router.post('/setmat',this.upload.single('test'), adminController.setMaterias);
    }
}
export default new AdminRoutes().router;