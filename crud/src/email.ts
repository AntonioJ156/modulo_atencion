
const nodemailer = require('nodemailer');
//Correo electronico
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      //user: 'api.atencion.coordinaciones@gmail.com',
      // pass: 'Sistemas.2021$'
      user: 'api_atencion_coordinaciones@queretaro.tecnm.mx',
      pass: 'Coordsis$2021'
    }
  });
class TheMailer {
  // Envio de correo por caso generado
  public NewMail(add:string,folio:string) {

    var mailOptions = {
      from: 'no-reply@queretaro.tecnm.mx',
      to: add,
      subject: 'Su caso se ha registrado con éxito.',
      text:'Para poder consultar el estatus de tu caso de atención por tu coordinacion, deberás ingresar nuevamente al módulo de atención en el siguiente link http://sii-apps.itq.edu.mx/ControlCoord/student/index'+
      ' Y colocar el siguiente folio:\n'+folio+'.\n\n\nSaludos cordiales.'
    };
    transporter.sendMail(mailOptions, function(error: any, info: { response: string; }){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  }
  //Actualizacion de casos
  public UpMail(add:string,folio:string) {
    var mailOptions = {
      from: 'no-reply@queretaro.tecnm.mx',
      to: add,
      subject: 'Actualización de Caso: '+folio,
      text:'El cordinador ha actualizado su caso: '+folio+'.\nConsulte su caso para mas detalles'
    };
    transporter.sendMail(mailOptions, function(error: any, info: { response: string; }){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  }

}
const maily = new TheMailer;
export default maily;
