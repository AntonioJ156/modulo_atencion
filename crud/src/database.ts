// importacion de librerias y llaves
import mysql from 'promise-mysql';
import keys from './keys';

const pooly  = mysql.createPool(keys.database);
/**
 * Conexión inicial con la base de datos
 */
pooly.getConnection()
    .then( async (connection: mysql.PoolConnection) => {
        (await pooly).releaseConnection(connection);
        console.log('DB is Connected');
    }).catch (error => {console.log("DB not live")});
export default pooly;