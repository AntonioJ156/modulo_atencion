import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { PagesModule } from './pages/pages.module';
import { AppComponent } from './app.component';
import { CaseServiceService } from './services/case-service.service';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { AuthGuard } from './gard/auth.guard';
import { ComponentsModule } from './components/components.module';
import { QnaService } from './services/qna.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ComponentsModule,
    PagesModule
  ],
  providers: [
    CaseServiceService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    QnaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
