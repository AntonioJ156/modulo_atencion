import { IQnA } from '../models/QnA';


export class QnA implements IQnA {
    id: number;
    question: string;
    answers: string[];

    constructor({ id, question, answers }) {
        this.id = id;
        this.question = question;
        this.answers = answers;
    }
}