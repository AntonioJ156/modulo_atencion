import { Injectable } from '@angular/core';
import { HttpInterceptor } from "@angular/common/http";
import { AuthService } from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(
    private authService: AuthService
  ) { }

  /* Agarra el token del usuario y le agrega la palabara 'Bearer' al inicio, le agrega el header 'Authorization' 
  y finalmente lo retorna en un Request*/
  intercept(req, next) {
    // console.log('dentro del token interceptor');
    const tokenizedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${this.authService.getToken()}`
      }
    })
    return next.handle(tokenizedReq);
  }

}
