import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { QnA } from '../classes/qns';

@Injectable({
  providedIn: 'root'
})
export class QnaService {

  private qnas: QnA[] = [];

  //API_URI = 'http://localhost:3000/admin/';
  API_URI = 'http://38.65.128.106:3000/admin/';


  // constructor(private http: HttpClient) {
  //   this.getQuestionsAnswers()
  //     .subscribe( (allQuestions:any) => {
  //       // console.log(allQuestions);
  //       allQuestions.forEach( singleQuestion => {
  //         // console.log(singleQuestion);
  //         this.qnas.push( new QnA(singleQuestion));
  //       });
  //       // console.log(this.qnas);
  //     });
  // }

  constructor(private http: HttpClient) {
    this.getQuestionsAnswers()
      .subscribe( (res: any) => {
        this.qnas = res;
        localStorage.setItem('qna', JSON.stringify(this.qnas));
        // console.log(this.qnas);
        // console.log(JSON.parse(localStorage.getItem('qna')));
      },
      err => {
        console.log(err);
      });
  }

  getQuestionsAnswers() {
    return this.http.get(`${this.API_URI}getqna`);
  }

  saveQuestionsAnswers(item) {
    return this.http.put(`${this.API_URI}setqna`, item);
  }

  public getQuestionById( id: number ) {
    this.qnas = JSON.parse(localStorage.getItem('qna'));
    // console.log(this.qnas);
    return this.qnas.find( (question) => question.id == id );
  }

  public updateQnA(newQnAs: QnA) {
    this.qnas = JSON.parse(localStorage.getItem('qna'));
    const questionIndex = this.qnas.findIndex( (r) => r.id === newQnAs.id );

    this.qnas[questionIndex] = newQnAs;

    localStorage.setItem('qna', JSON.stringify(this.qnas));
  }

}
