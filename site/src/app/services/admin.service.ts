import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  //API_URI = 'http://localhost:3000';
  API_URI = 'http://38.65.128.106:3000';

  constructor(private http: HttpClient) {
  }

  public uploadAlumnsFile( fileToUpload: File ) {
    const formData: FormData = new FormData();
    formData.append('test', fileToUpload, fileToUpload.name);

    return this.http.post<any>(`${this.API_URI}/admin/setstu`, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

  public updateMaterias( fileToUpload: File ) {
    const formData: FormData = new FormData();
    formData.append('test', fileToUpload, fileToUpload.name);

    return this.http.post<any>(`${this.API_URI}/admin/setmat`, formData);
  }

  public deleteCases() {
    return this.http.delete(`${this.API_URI}/admin/delcase`);
  }

  public getAllCoords() {
    return this.http.get(`${this.API_URI}/cord/users`);
  }

  public addCoord(newCoord) {
    return this.http.post(`${this.API_URI}/cord/register`, newCoord);
  }

  public updateCoord(updatedCoord) {
    return this.http.post(`${this.API_URI}/cord/updatecord`, updatedCoord);
  }

  public delCoord(delCoord) {
    return this.http.post(`${this.API_URI}/cord/deletecord`, delCoord);
  }
}
