import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http';
import { Case } from '../models/Case';

@Injectable({
  providedIn: 'root'
})
export class CaseServiceService {

  //API_URI = 'http://localhost:3000/';
  API_URI = 'http://38.65.128.106:3000/';

  constructor(private http: HttpClient) { }

  getSingleCase(id: string) {
    return this.http.get(`${this.API_URI}student/${id}`);
    // return this.http.get(`${this.API_URI}/games/${id}`);
  }

  newCase(caseNew: Case) {
    return this.http.post(`${this.API_URI}student/`, caseNew);
  }

  getQs() {
    return this.http.get(`${this.API_URI}student/q`);
  }

  getCareers() {
    return this.http.get(`${this.API_URI}student/crs`); ////                      ARMANDO WAS HERE
  }

  getAnswers(id: number) {
    return this.http.get(`${this.API_URI}cord/answers/${id}`);
  }

  getAllCases() {
    return this.http.get(`${this.API_URI}cord/cases`);
  }

  updateCases(id: string | number, updatedCase) {
    return this.http.put(`${this.API_URI}cord/update/${id}`, updatedCase);
  }

  getMaterias( item1: any, item2: any) {
    return this.http.post(`${this.API_URI}student/m`, {
      career: item1.toString(),
      semester: item2.toString()
    });
  }

  // getStudent( id: string, email: string) {
  //   return this.http.get(`${this.API_URI}student/s/${id}/${email}`);
  // }

  getStudent( stu_data: any) {
    return this.http.post(`${this.API_URI}student/s`, stu_data);
  }

}
