import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //private URL = 'http://localhost:3000/cord';
  private URL = 'http://38.65.128.106:3000/cord';

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  register(user) {
    return this.http.post<any>(this.URL + '/register', user);
  }

  signIn(user) {
    return this.http.post<any>(this.URL + '/signin', user);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  logOut() {
    localStorage.removeItem('token');
    this.router.navigate(['/coord/login']);
  }
  getUsers() {
    return this.http.get(this.URL + '/users');
  }

}
