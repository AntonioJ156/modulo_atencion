import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Components
import { ConsultaCasoComponent } from './pages/student/consulta-caso/consulta-caso.component';
import { InicioComponent } from './pages/student/inicio/inicio.component';
import { CoordLoginComponent } from './pages/coord/coord-login/coord-login.component';

import { COORD_ROUTES } from './pages/coord/coord.routes';
import { STUDENT_ROUTES } from './pages/student/student.routes';


const routes: Routes = [
  
  { path: 'student/index', component: InicioComponent},
  { path: 'student', children: STUDENT_ROUTES},
  { path: 'coord', children: COORD_ROUTES },
  { path: '**', pathMatch: 'full', redirectTo: 'student/index' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
