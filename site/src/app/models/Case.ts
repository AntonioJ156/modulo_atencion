export interface Case {

  case_folio?: string;
  alum_id?: number;
  alum_name?: string;
  alum_email?: string;
  coord_name?: string;
  cord_id?: number;
  q_cont?: string;
  q_obs?: string;

}


export interface CaseP {

  case_folio?: string;
  alum_id?: number;
  alum_name?: string;
  alum_email?: string;
  coord_name?: string;
  cord_id?: number;
  q_cont?: string;
  q_obs?: string;
  a_cont?: string;
  a_obs?: string;
  case_pro?: string;
  case_status?: boolean;
  case_date?: Date;

}
