export interface IQnA {
    id: number;
    question: string;
    answers: string[];
}