export interface ICoordinador {
    id: number;
    name: string;
    email: string;
    carreras: string;
    password: string;
}