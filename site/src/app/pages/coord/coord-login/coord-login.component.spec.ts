import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoordLoginComponent } from './coord-login.component';

describe('CoordLoginComponent', () => {
  let component: CoordLoginComponent;
  let fixture: ComponentFixture<CoordLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoordLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
