import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { CaseServiceService } from '../../../services/case-service.service';
import { Router  } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-coord-login',
  templateUrl: './coord-login.component.html',
  styleUrls: ['./coord-login.component.scss']
})
export class CoordLoginComponent implements OnInit {

  user = {
    cord_email: '',
    cord_pass: ''
  }
  opts: any = [
    {
      cord_name : ''
    }
  ];

  constructor(
    private authService: AuthService,
    private router: Router,
    private caseService: CaseServiceService,
  ) { }

  ngOnInit() {
    this.authService.getUsers()
      .subscribe(
        res => {
          // console.log(res + ' MIRAME');
          this.opts = res;
        },
        err => console.log(err)
      );
  }

  signIn() {
    // console.log(this.user);
    this.authService.signIn(this.user)
      .subscribe(
        res => {
          //console.log(res);
          localStorage.setItem('token', res.token); // guarda el token en el local storage

          if( res.coordRole == "99" ) {
            this.router.navigateByUrl('coord/admin/index');
          } else {
            this.router.navigateByUrl('coord/casos');
          }

        },
        err => {
          console.log(err);
          Swal.fire({
            icon: 'error',
            title: 'Error al inciar sesion',
            text: err.error
          });
        }
      );
  }


}
