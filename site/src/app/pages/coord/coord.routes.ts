import { Routes } from '@angular/router';

import { AllCasosComponent } from './all-casos/all-casos.component';
import { EditarCasoComponent } from './editar-caso/editar-caso.component';
import { CoordLoginComponent } from './coord-login/coord-login.component';

import { AuthGuard } from '../../gard/auth.guard';
import { ADMIN_ROUTES } from './admin/admin.routes';



export const COORD_ROUTES: Routes = [

    { path: 'login', component: CoordLoginComponent },
    { path: 'casos', component: AllCasosComponent, canActivate: [AuthGuard] },
    { path: 'casos/edit/:folio_caso', component: EditarCasoComponent,  canActivate: [AuthGuard] },
    { path: 'admin', children: ADMIN_ROUTES,  canActivate: [AuthGuard] },
    { path: '**', pathMatch: 'full', redirectTo: 'login' },
    
]