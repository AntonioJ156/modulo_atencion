import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ICoordinador } from 'src/app/models/Coordinador';
import { AdminService } from 'src/app/services/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  titulo = 'Panel de administrador';

  public coordinadorForm: FormGroup;
  coordinadores: any;
  loading = false;

  constructor(
    private adminService: AdminService,
    private router: Router
  ) { }

  ngOnInit() {
    this.adminService.getAllCoords()
      .subscribe(
        res => {
          this.coordinadores = res;
          //console.log(res);
        },
        err => {
          console.log(err);
        }
      );
  }

  addCoordinador() {
    let aux = {
      cord_id: this.coordinadores.length,
      cord_name: '',
      cord_email: '',
      cord_pass: '',
      cord_crs: ''
    };
    Swal.fire({
      title: `Añadir coordinador`,
      text: "Deseas añadir coordinador?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.coordinadores.push(aux);
        this.adminService.addCoord(aux)
    .subscribe(
      res => {
            this.router.navigate(['/coord/admin/usuarios', aux.cord_id]);
            //console.log(res);
          },
          err => {
            console.log(err);
          }
        );
      }else{
        location.reload();
      }
    });
    //console.log(this.coordinadores);

    

    // console.log(this.qna);
  }

  deleteCoord(index: number): void {

    //console.log(this.qna);

    Swal.fire({
      title: `¿Está seguro que quiere borrar el coordinador ${index}?`,
      text: "Esta acción no se puede revertir",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.adminService.delCoord({
            cord_id: index
          }) .subscribe(
            (res) => {
              Swal.fire({
                icon: 'success',
                title: 'Listo',
                text: `Coordinador eliminado con éxito`
              });
              location.reload();
            },
            (err) => {
              Swal.fire({
                icon: 'error',
                title: 'Error al eliminar el coordinador',
                text: err.error.message
              });
            });
      }
    });
  }

  reload(){
    location.reload();
  }
}
