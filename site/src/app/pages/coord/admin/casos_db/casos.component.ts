import { Component, OnInit } from '@angular/core';
import { CaseServiceService } from 'src/app/services/case-service.service';
import { CsvMakerService } from '../../../../services/csv-maker.service';
import Swal from 'sweetalert2';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-casos',
  templateUrl: './casos.component.html',
  styleUrls: ['./casos.component.scss']
})
export class CasosDbComponent implements OnInit {

  titulo = 'Panel de Administrador';
  loading = false;
  allCases: any = [];

  btn1Pulsed = false;
  btn2Pulsed = false;

  constructor(
    private caseService: CaseServiceService,
    private adminService: AdminService
  ) { }

  ngOnInit() {
  }

  stepOne() {
    this.btn1Pulsed = true;
    this.getCSV();
  }

  getCSV(){

    this.caseService.getAllCases()
      .subscribe(
        res => {
          //console.log(res);
          this.allCases = res;
          this.loading = true;
          setTimeout(() => this.loading = false, 4000);
          CsvMakerService.exportToCsv('report.csv', this.allCases);
        },
        err => console.log(err)
      );

  }

  stepTwo() {
    if ( this.btn1Pulsed ) {
      this.btn2Pulsed = true;
      this.adminService.deleteCases()
        .subscribe( res => {
          //console.log(res);
          Swal.fire({
            icon: 'success',
            title: 'Se a vaciado la base de datos',
            showConfirmButton: false,
          })
        },
        err => {
          console.log(err);
        });
    } else {
        Swal.fire({
          icon: 'error',
          title: 'No puedes hacer esto ahora',
          text: 'Primero tiene que guardar un respaldo como se indica en el paso 1'
        });
    }
  }

}
