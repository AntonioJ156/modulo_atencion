import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { QnA } from 'src/app/classes/qns';
import { QnaService } from 'src/app/services/qna.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-qna',
  templateUrl: './edit-qna.component.html',
  styleUrls: ['./edit-qna.component.scss']
})
export class EditQnaComponent implements OnInit {

  header = 'Panel de Administrador';
  qna: QnA;
  loading = false;
  private subscriptions: Subscription[] = [];
  public questionForm: FormGroup;
  private answers: FormArray;
  public params: any;

  constructor(
    private route: ActivatedRoute,
    private qnaService: QnaService,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) { }

  // ngOnInit() {
  //   this.subscriptions.push(
  //     this.route.paramMap.subscribe( params => {
  //       const qId = params.get('id');
  //       this.qna = this.qnaService.getQuestionById(parseInt(qId));
  //       console.log(this.qna);
  //       this.createForm();
  //     })
  //   );
  // }

  ngOnInit() {
    this.params = this.activeRoute.snapshot.params;
    // console.log(this.params);
    this.qna = this.qnaService.getQuestionById(this.params.id);
    // console.log(this.qna);
    this.createForm();
  }

  private createForm() {
    this.questionForm = this.fb.group({
      question: [this.qna.question, [Validators.required]],
      answers: this.fb.array([])
    });

    this.answers = this.questionForm.get('answers') as FormArray;

    this.qna.answers.forEach( ans => {
      this.answers.push(this.createAnswer(ans));
    });

  }

  private createAnswer( answer: string ): FormGroup {
    return this.fb.group({
      uniqueAnswer: [answer, [Validators.required]]
    });
  }

  addAnswer(): void {
    this.answers.push(this.createAnswer(''));
  }

  deleteAnswer(index: number): void {
    Swal.fire({
      title: '¿Está seguro que quiere borrar esta respuesta?',
      text: "No se puede revertir esto si guarda la pregunta",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar respuesta',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        const arrayControl = this.questionForm.controls['answers'] as FormArray;
        arrayControl.removeAt(index);
        Swal.fire(
          'Borrada!',
          'Respuesta borrada.',
          'success'
        )
      }
    });
  }

  submitForm() {
    if ( this.questionForm.valid ) {

      Swal.fire({
        allowOutsideClick: false,
        icon: 'info',
        text: 'Espere por favor...'
      });
      Swal.showLoading();

      const {question, answers} = this.questionForm.value;
      const filteredAnswers = answers.map(item => item.uniqueAnswer);
      this.qnaService.updateQnA(
        new QnA(
          {
            id: this.qna.id,
            question,
            answers: filteredAnswers
          }
        )
      );

      this.qnaService.saveQuestionsAnswers(JSON.parse(localStorage.getItem('qna')))
        .subscribe(
          res => {
            //console.log(res);

            Swal.fire({
              icon: 'success',
              title: 'Listo',
              text: 'Respuestas guardadas'
            });

            this.router.navigate(['/coord/admin/preguntas']);
          },
          err => {
            console.log(err);
          }
        );

    } else {
      console.log("Form Error");
    }
  }

  reload() {
    location.reload();
  }

}
