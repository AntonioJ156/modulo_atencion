import { Routes } from '@angular/router';

import { AllCasosComponent } from './../all-casos/all-casos.component';
import { EditarCasoComponent } from './../editar-caso/editar-caso.component';
import { CoordLoginComponent } from './../coord-login/coord-login.component';
import { AdminIndexComponent } from './admin-index/admin-index.component';

import { AuthGuard } from '../../../gard/auth.guard';
import { AlumnosDbComponent } from './alumnos_db/alumnos.component';
import { MateriasDbComponent } from './materias_db/materias.component';
import { CasosDbComponent } from './casos_db/casos.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ViewQnaComponent } from './view-qna/view-qna.component';
import { EditQnaComponent } from './edit-qna/edit-qna.component';
import { EditUsuariosComponent } from './edit-usuarios/edit-usuarios.component';



export const ADMIN_ROUTES: Routes = [

    { path: 'index', component: AdminIndexComponent},
    { path: 'casos', component: AllCasosComponent },
    { path: 'preguntas', component: ViewQnaComponent },
    { path: 'preguntas/:id', component: EditQnaComponent },
    { path: 'alumnos', component: AlumnosDbComponent },
    { path: 'materias', component: MateriasDbComponent },
    { path: 'cleancasos', component: CasosDbComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'usuarios/:id', component: EditUsuariosComponent },
    // { path: 'casos/edit/:folio_caso', component: EditarCasoComponent,  canActivate: [AuthGuard]},
    { path: '**', pathMatch: 'full', redirectTo: 'index' },

]