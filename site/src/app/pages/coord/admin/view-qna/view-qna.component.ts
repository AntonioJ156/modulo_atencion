import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { stringify } from 'querystring';
import { QnA } from 'src/app/classes/qns';
import { IQnA } from 'src/app/models/QnA';
import { QnaService } from 'src/app/services/qna.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-qna',
  templateUrl: './view-qna.component.html',
  styleUrls: ['./view-qna.component.scss']
})
export class ViewQnaComponent implements OnInit{

  header = 'Panel de Administrador';
  qna: IQnA[];
  loading = false;

  constructor(
    private qnaService: QnaService,
    private fb: FormBuilder,
    private router: Router
  ) { }


  ngOnInit() {
    setTimeout(() => {
      this.qna = JSON.parse(localStorage.getItem('qna'));
    }, 500);
    // this.qna = JSON.parse(localStorage.getItem('qna'));
  }


  // ngOnDestroy() {
  //   localStorage.removeItem('qna');
  // }

  deleteQuestion(index: number): void {

    //console.log(this.qna);

    Swal.fire({
      title: '¿Está seguro que quiere borrar esta pregunta?',
      text: "Esta acción no se puede revertir si guarda todos los cambios",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar pregunta',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.qna.splice(index, 1);
        localStorage.setItem('qna', JSON.stringify(this.qna));

        Swal.fire(
          'Borrada!',
          'Pregunta borrada.',
          'success'
        );
      }
    });
  }

  addQuestion() {
    //console.log(this.qna.length);
    let aux: QnA = {
      id: this.qna.length + 1,
      question: '',
      answers: []
    };
    this.qna.push(aux);

    localStorage.setItem('qna', JSON.stringify(this.qna));

    this.qnaService.saveQuestionsAnswers(JSON.parse(localStorage.getItem('qna')))
        .subscribe(
          res => {
            //console.log(res);
            this.router.navigate(['/coord/admin/preguntas', aux.id]);
          },
          err => {
            console.log(err);
          }
        );

    //console.log(this.qna);
  }

  submitForm() {


    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.qnaService.saveQuestionsAnswers(JSON.parse(localStorage.getItem('qna')))
        .subscribe(
          res => {
            //console.log(res);

            Swal.fire({
              icon: 'success',
              title: 'Listo',
              text: 'Preguntas guardadas'
            });

          },
          err => {
            console.log(err);
          }
        );

  }

  reload() {
    location.reload();
  }

}
