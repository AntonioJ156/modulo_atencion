import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {HttpEventType, HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { AdminService } from 'src/app/services/admin.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.scss']
})
export class AlumnosDbComponent implements OnInit {

  titulo = 'Panel de Administrador';
  fileToUpload: File = null;

  constructor(
    private adminService: AdminService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initialAlert();
  }

  initialAlert() {

    Swal.fire({
      title: 'Información delicada',
      text: 'Este es un panel para actualizar información importante.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Regresar'
    }).then((result) => {
      //console.log(result);
      if (result.isConfirmed) {

      } else {
        this.router.navigate(['/coord/admin/index']);
      }
    });

  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    //console.log(this.fileToUpload);
  }

  onSubmit() {
    this.adminService.uploadAlumnsFile(this.fileToUpload)
      .subscribe(
      res => {
        //console.log(res);
        Swal.fire({
          icon: 'success',
          title: 'Listo',
          text: 'Información de alumnos actualizada exitosamente'
        });
        this.router.navigateByUrl('/coord/admin/index');
      },
      err => {
        //console.log(err);
        Swal.fire({
          icon: 'error',
          title: 'Error al generar tu caso',
          text: err.error.message
        });
      });
  }

}
