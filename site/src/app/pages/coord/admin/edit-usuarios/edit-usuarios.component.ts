import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
//import { element } from 'protractor';
import { AdminService } from 'src/app/services/admin.service';
import { CaseServiceService } from 'src/app/services/case-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-usuarios',
  templateUrl: './edit-usuarios.component.html',
  styleUrls: ['./edit-usuarios.component.scss']
})
export class EditUsuariosComponent implements OnInit {

  header = 'Panel de administrador';
  loading = false;
  public newPass = false;
  coordinadores: any;
  coordinador: any;
  carreras: any;
  private params: any;
  public coordinadorForm: FormGroup;

  constructor(
    private adminService: AdminService,
    private caseService: CaseServiceService,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this.params = this.activeRoute.snapshot.params;
    this.adminService.getAllCoords()
      .subscribe(
        res => {
          this.coordinadores = res;
          this.coordinador = this.getCoordinadorById(this.params.id);
          //console.log(this.coordinador);
          this.createForm();
        },
        err => {
          console.log(err);
        }
      );
  }
  createForm() {
    this.coordinadorForm = this.fb.group({
        nombre: [this.coordinador.cord_name],
        correo: [this.coordinador.cord_email],
        contraseña: [this.coordinador.cord_pass],
        carreras: [this.coordinador.cord_crs]
    });
    this.getCarreras();
  }

  getCarreras() {
    this.caseService.getCareers()
      .subscribe(
        res => {
          this.carreras = res;
          //console.log(res);
        },
        err => {
          console.log(err);
        }
      )
  }

  checaCarrera(id) {
    let aux = this.coordinadorForm.value.carreras.split(',');

    if ( aux.find((numberCareer) => numberCareer == id )) {
      return true;
    } else {
      return false;
    }

  }

  actualizaCarrerra(id) {
    let aux: string[] = this.coordinadorForm.value.carreras.split(',');
    // console.log(aux);
    // console.log(id);

    if ( aux.find((numberCareer) => numberCareer == id )) {

      // console.log(aux);
      aux = aux.filter( element => element != id );
      // console.log(aux);
      let auxCarrer = '';
      aux.forEach( (element) => {
        auxCarrer += `${element},`;
      });
      auxCarrer = auxCarrer.replace(/(^,)|(,$)/g, "")
      this.coordinadorForm.controls['carreras'].patchValue(auxCarrer);
      // console.log(aux);

    } else {

      aux.push(id.toString());
      aux.sort();
      let auxCarrer = '';
      aux.forEach( (element) => {
        auxCarrer += `${element},`;
      });
      auxCarrer = auxCarrer.replace(/(^,)|(,$)/g, "")
      this.coordinadorForm.controls['carreras'].patchValue(auxCarrer);

    }

  }

  getCoordinadorById( id: number ) {
    return this.coordinadores.find( (cord) => cord.cord_id == id);
  }

  checkboxSelected( id: number ) {
    console.log(id);
  }

  nuevoPass() {
    this.newPass = true;
    this.coordinadorForm.controls['contraseña'].patchValue('');
  }

  submitForm() {
    //console.log(this.coordinadorForm.get('contraseña').value);
    //console.log(this.newPass);

    if ( this.coordinadorForm.valid ) {

      Swal.fire({
        allowOutsideClick: false,
        icon: 'info',
        text: 'Espere por favor...'
      });
      Swal.showLoading();

      if ( !this.newPass ) {

        this.adminService.updateCoord({
          cord_id: this.coordinador.cord_id,
          cord_name: this.coordinadorForm.get('nombre').value,
          cord_email: this.coordinadorForm.get('correo').value,
          cord_crs:  this.coordinadorForm.get('carreras').value,
        }).subscribe( res => {
          Swal.fire({
            icon: 'success',
            title: 'Listo',
            text: 'Información del cordinador actualizada'
          });
          this.router.navigateByUrl('/coord/admin/usuarios');
        },
        err => {
          console.log(err);
          Swal.fire({
            icon: 'error',
            title: 'Error al actualizar información del coordinador',
            text: err.error.message
          });
        });

      } else {

        this.adminService.updateCoord({
          cord_id: this.coordinador.cord_id,
          cord_name: this.coordinadorForm.get('nombre').value,
          cord_email: this.coordinadorForm.get('correo').value,
          cord_pass: this.coordinadorForm.get('contraseña').value,
          cord_crs:  this.coordinadorForm.get('carreras').value,
        }).subscribe( res => {
          Swal.fire({
            icon: 'success',
            title: 'Listo',
            text: 'Información del cordinador actualizada'
          });
          this.router.navigateByUrl('/coord/admin/usuarios');
        },
        err => {
          console.log(err);
          Swal.fire({
            icon: 'error',
            title: 'Error al actualizar información del coordinador',
            text: err.error.message
          });
        });
      }

    }

  }

  reload() {
    location.reload();
  }

}
