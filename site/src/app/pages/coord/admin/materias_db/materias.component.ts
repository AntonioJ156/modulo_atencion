import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-materias',
  templateUrl: './materias.component.html',
  styleUrls: ['./materias.component.scss']
})
export class MateriasDbComponent implements OnInit {

  titulo = 'Panel de Administrador';
  fileToUpload: File = null;

  constructor(
    private adminService: AdminService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initialAlert();
  }

  initialAlert() {

    Swal.fire({
      title: 'Información delicada',
      text: 'Este es un panel para actualizar información importante. Se recomienda actualizar unicamente si hubo cambios en las mterias de alguna carrera.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Regresar'
    }).then((result) => {
      //console.log(result);
      if (result.isConfirmed) {
        Swal.fire(
          'Se encuentra a punto de modificar la base de datos de materias',
          '',
          'warning'
        )
      } else {
        this.router.navigate(['/coord/admin/index']);
      }
    });

  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    //console.log(this.fileToUpload);
  }

  onSubmit() {
    this.adminService.updateMaterias(this.fileToUpload)
      .subscribe(
      res => {
        //console.log(res);
        Swal.fire({
          icon: 'success',
          title: 'Listo',
          text: 'Información de materia actualizada exitosamente'
        });
        this.router.navigateByUrl('/coord/admin/index');
      },
      err => {
        console.log(err);
        Swal.fire({
          icon: 'error',
          title: 'Error al generar tu caso',
          text: err.error.message
        });
      });
  }

}
