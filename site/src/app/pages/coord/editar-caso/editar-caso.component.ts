import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CaseServiceService } from '../../../services/case-service.service';
import { Location } from '@angular/common';
import { CaseP } from 'src/app/models/Case';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { async } from '@angular/core/testing';



@Component({
  selector: 'app-editar-caso',
  templateUrl: './editar-caso.component.html',
  styleUrls: ['./editar-caso.component.scss']
})
export class EditarCasoComponent implements OnInit {

  public forma: FormGroup;

  opts: any = [];

  case: CaseP = {
    case_folio: '',
    alum_id: null,
    alum_name: '',
    alum_email: '',
    coord_name: '',
    cord_id: null,
    q_cont: '',
    q_obs: '',
    a_cont: '',
    a_obs: '',
    case_pro: '',
    case_status: false
   };


  params: any;

  onceFlag = false;

  answers: any = [];

  resueltoColor: boolean;

  preguntaNo: any;


  constructor(
    private caseService: CaseServiceService,
    private router: Router,
    private location: Location,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {

    this.cargaFormulario();
    this.getQuestions();

  }


  cargaFormulario() {

    // console.log('CARGANDO FORMULARIOS');

    this.forma = this.fb.group({
      folio: [{value: '', disabled: true}],
      no_control: [{value: '', disabled: true}],
      nombre: [{value: '', disabled: true}],
      correo: [{value: '', disabled: true}],
      carrera: [{value: '', disabled: true}],
      pregunta: [''],
      observaciones: [''],
      estado: [''],
      respuesta: [''],
      observaciones_coord: [''],
      resuelto: ['']
    });

  }

  getQuestions(){
    // console.log('OBTENIENDO PREGUNTAS');
    this.caseService.getQs().subscribe(
      res => {
        this.opts = res;
        // console.log(this.opts);
        // console.log('RESPUESTAS OBTENIDAS');
        this.getCase();
      },
      err => console.log(err)
    );
  }

  getCase(){
    this.params = this.activeRoute.snapshot.params;
    // console.log(this.params);
    this.caseService.getSingleCase(this.params.folio_caso).subscribe(
      res => {
        this.case = res;
        // console.log(this.case);
        this.resueltoColor = this.case.case_status;
        // console.log(this.case);
        this.findAnswers( this.findInitalQuestionId() );
      },
      err => console.error(err)
    );
  }

  findInitalQuestionId(): any {
    for( let i = 0 ; i < this.opts.length; i++ ){
      if ( this.opts[i].q_cont === this.case.q_cont) {
        this.preguntaNo = i;
        return i + 1;
      }
    }
  }

  loadData(question_id: number) {
    // console.log('CARGANDO DATOS');
    this.forma.controls.folio.patchValue(this.case.case_folio);
    this.forma.controls.no_control.patchValue(this.case.alum_id);
    this.forma.controls.nombre.patchValue(this.case.alum_name);
    this.forma.controls.correo.patchValue(this.case.alum_email);
    this.forma.controls.carrera.patchValue(this.case.coord_name);
    this.forma.controls.pregunta.patchValue(this.opts[question_id]);
    this.forma.controls.observaciones.patchValue(this.case.q_obs);
    this.forma.controls.estado.patchValue(this.case.case_pro);
    // console.log('JUSTO ANTES DE ASIGNAR RESPUESTA POR DEFECTO');
    this.forma.controls.respuesta.patchValue(this.answers[this.findInitialAnswerId()]);
    this.forma.controls.observaciones_coord.patchValue(this.case.a_obs);
    this.forma.controls.resuelto.patchValue(this.case.case_status);
  }

  findAnswers( id: number): any {
    this.caseService.getAnswers(id)
    .subscribe(
      res => {
        this.answers = res;
        // console.log(this.answers);
        if ( !this.onceFlag ){
          this.loadData(this.preguntaNo);
          this.onceFlag = true;
        }
      },
      err => console.log(err)
    );
    // console.log(this.onceFlag);
  }

  findQuestionId(): any{
    for( let i = 0 ; i < this.opts.length; i++ ){
      if ( this.opts[i].q_cont === this.forma.value.pregunta.q_cont) {
        this.findAnswers(this.opts[i].q_id);
      }
    }
  }


  findInitialAnswerId() {
    for( let i = 0 ; i < this.answers.length; i++ ){
      if ( this.answers[i].a_cont === this.case.a_cont) {
        return i;
      }
    }
  }


  saveCase() {
    if (this.forma.value.respuesta == null || this.forma.get('estado').value == "") {
      Swal.fire({
        icon: 'warning',
        title: 'Advertencia',
        text: 'Por favor llene los campos de Respuesta y Estado para poder guardar el caso.'
      });
      return
    }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.case = {
      case_folio: this.case.case_folio,
      alum_id: this.case.alum_id,
      alum_name: this.case.alum_name,
      alum_email: this.case.alum_email,
      coord_name: this.case.coord_name,
      cord_id: this.case.cord_id,
      q_cont: this.forma.value.pregunta.q_cont,
      q_obs: this.forma.get('observaciones').value,
      a_cont: this.forma.value.respuesta.a_cont,
      // a_cont: this.forma.get('respuesta').value,
      a_obs: this.forma.get('observaciones_coord').value,
      case_pro: this.forma.get('estado').value,
      case_status: this.forma.get('resuelto').value,
    }

    // console.log(this.case);
    this.caseService.updateCases(this.case.case_folio, this.case)
      .subscribe(
        res => {
          Swal.fire({
            icon: 'success',
            title: 'Listo',
            text: 'Respuestas guardadas'
          });
          // console.log(res);
          this.router.navigate(['/coord/casos']);
        },
        err => {
          console.error(err);
          Swal.fire({
            icon: 'error',
            title: 'Error al guardar respuestas',
            text: err.error.message
          });
        }
      );
  }

}
