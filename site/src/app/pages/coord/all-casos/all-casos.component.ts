import { Component, OnInit } from '@angular/core';
import { CaseServiceService } from '../../../services/case-service.service';
import { CaseP } from 'src/app/models/Case';
import { CsvMakerService } from '../../../services/csv-maker.service';



@Component({
  selector: 'app-all-casos',
  templateUrl: './all-casos.component.html',
  styleUrls: ['./all-casos.component.scss']
})
export class AllCasosComponent implements OnInit {

  header = 'Panel de Coordinador';

  flt: CaseP = {
    alum_name: '',
    q_cont: '',
    q_obs: '',
    a_cont: '',
    a_obs: '',
    case_pro: '',
    case_status: null,
    case_date: null
   };

  opts: any = [
    {
      q_id: '',
      q_cont: ''
    }
  ];

  answers: any = [
    {
      a_cont: ''
    }
  ];
  public qId: number;

  loading = false;

  constructor(
    private caseService: CaseServiceService,
    ) { }

  allCases: any = [];
  fltCases: any = [];
  headElements = ['No. Control', 'Nombre', 'Carrera', 'Pregunta', 'Fecha', 'Revisado', 'Consultar caso'];

  ngOnInit() {
    this.caseService.getAllCases()
      .subscribe(
        res => {
          //console.log(res);
          this.allCases = res;
          this.allCases.forEach(element => {
            var caseDate = new Date(element.case_date)
            if (element.case_status) {
              element.case_checked = 'Revisado'
            } else {
              var now = new Date()
              var dateDiff = now.getTime() - caseDate.getTime()
              var days = Math.floor(dateDiff / (1000*60*60*24))

              if (days < 7) {
                element.case_checked = 'Sin revisar - en tiempo'
              } else if (days < 15) {
                element.case_checked = 'Sin revisar - más de 7 días'
              } else {
                element.case_checked = 'Sin revisar - más de 15 días'
              }
            }

            element.case_date = caseDate.toLocaleString()
          });
          //this.allCases = this.allCases.reverse();
          //console.log(this.allCases);
          this.filterUpdate();
          // console.log(this.fltCases);
        },
        err => console.log(err)
      );
    this.caseService.getQs()
      .subscribe(
        res => {
          // console.log(res);
          this.opts = res;
          this.findAnswers();
        },
        err => console.log(err)
      );


  }

   findByTemplate(objects: Array<any>, template: any) {
    return objects.filter((obj: any) => {
        return Object.keys(template).every(propertyName => obj[propertyName] === template[propertyName]
          || template[propertyName] === '' || template[propertyName] === null ||
         ( (template[propertyName] as string && obj[propertyName] as string) ?
         obj[propertyName].includes(template[propertyName]) :
         obj[propertyName] === template[propertyName] )
          );
    });
  }

  filterUpdate(){
    this.flt.alum_name = this.flt.alum_name.toUpperCase();
    //console.log( String( this.flt.case_date ) );
    this.fltCases = this.findByTemplate(this.allCases,this.flt);
    // console.log(this.flt.case_status&&true)
  }

  findQuestionId(): any {
    for (let i = 0 ; i < this.opts.length; i++) {
      if (this.opts[i].q_cont === this.flt.q_cont) {
        return i + 1;
      }
    }
  }

  findAnswers(): any {
    this.qId =  this.findQuestionId();
    //console.log(this.qId);
    this.caseService.getAnswers(this.qId)
    .subscribe(
      res => {
        //console.log(res);
        this.answers = res;
      },
      err => console.log(err)
    );
  }

  getCSV(){
    this.loading = true;
    CsvMakerService.exportToCsv('report.csv', this.fltCases);
    setTimeout(() => this.loading = false, 4000);

  }


}
