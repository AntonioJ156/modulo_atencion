import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCasosComponent } from './all-casos.component';

describe('AllCasosComponent', () => {
  let component: AllCasosComponent;
  let fixture: ComponentFixture<AllCasosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllCasosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCasosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
