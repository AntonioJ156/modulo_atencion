import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultaCasoComponent } from './student/consulta-caso/consulta-caso.component';
import { InicioComponent } from './student/inicio/inicio.component';
import { CoordLoginComponent } from './coord/coord-login/coord-login.component';
import { AllCasosComponent } from './coord/all-casos/all-casos.component';
import { EditarCasoComponent } from './coord/editar-caso/editar-caso.component';
import { GenerarCasoComponent } from './student/generar-caso/generar-caso.component';
import { AdminIndexComponent } from './coord/admin/admin-index/admin-index.component';
import { AlumnosDbComponent } from './coord/admin/alumnos_db/alumnos.component';
import { MateriasDbComponent } from './coord/admin/materias_db/materias.component';
import { CasosDbComponent } from './coord/admin/casos_db/casos.component';
import { UsuariosComponent } from './coord/admin/usuarios/usuarios.component';
import { ComponentsModule } from '../components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { EditQnaComponent } from './coord/admin/edit-qna/edit-qna.component';
import { ViewQnaComponent } from './coord/admin/view-qna/view-qna.component';
import { EditUsuariosComponent } from './coord/admin/edit-usuarios/edit-usuarios.component';


@NgModule({
  declarations: [
    ConsultaCasoComponent,
    InicioComponent,
    CoordLoginComponent,
    AllCasosComponent,
    EditarCasoComponent,
    GenerarCasoComponent,
    AdminIndexComponent,
    AlumnosDbComponent,
    MateriasDbComponent,
    CasosDbComponent,
    UsuariosComponent,
    EditQnaComponent,
    ViewQnaComponent,
    EditUsuariosComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  exports: [
    ConsultaCasoComponent,
    InicioComponent,
    CoordLoginComponent,
    AllCasosComponent,
    EditarCasoComponent,
    GenerarCasoComponent,
    AdminIndexComponent,
    AlumnosDbComponent,
    MateriasDbComponent,
    CasosDbComponent,
    UsuariosComponent
  ]
})
export class PagesModule{}
