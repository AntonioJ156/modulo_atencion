import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaCasoComponent } from './consulta-caso.component';

describe('ConsultaCasoComponent', () => {
  let component: ConsultaCasoComponent;
  let fixture: ComponentFixture<ConsultaCasoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaCasoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaCasoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
