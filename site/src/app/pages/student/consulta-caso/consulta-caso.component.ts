import { Component, OnInit } from '@angular/core';
import { CaseP } from 'src/app/models/Case';
import { CaseServiceService } from '../../../services/case-service.service';
import { Location } from '@angular/common';

import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-consulta-caso',
  templateUrl: './consulta-caso.component.html',
  styleUrls: ['./consulta-caso.component.scss']
})
export class ConsultaCasoComponent implements OnInit {

  header = 'Atención a Alumnos';

  case: CaseP = {
    case_folio: '',
    alum_id: 0,
    alum_name: '',
    alum_email: '',
    coord_name: '',
    q_cont: '',
    q_obs: '',
    a_cont: '',
    a_obs: '',
    case_pro: '',
    case_status: false
  };

  forma: FormGroup;

  constructor(
    private caseServ: CaseServiceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private location: Location
    ) { }

  ngOnInit() {
    this.cargaFormulario();
    const params = this.activatedRoute.snapshot.params;
    if (params.folio) {
      this.caseServ.getSingleCase(params.folio)
      .subscribe(
        res => {
          // console.log(res);
          this.case = res;
          this.loadData();
          this.generaAlerta();
        },
        err => {
          // console.log(err)
          Swal.fire(
            'Caso no encontrado',
            'Por favor revisa tu número de folio',
            'warning'
          ).then( (value) => {
            this.back()
          })
        }
      );
    }
    // console.log(this.case);
  }

  cargaFormulario() {

    this.forma = this.fb.group({
      folio: [{value: '', disabled: true}],
      no_control: [{value: '', disabled: true}],
      nombre: [{value: '', disabled: true}],
      correo: [{value: '', disabled: true}],
      carrera: [{value: '', disabled: true}],
      pregunta: [{value: '', disabled: true}],
      observaciones: [{value: '', disabled: true}],
      estado: [{value: '', disabled: true}],
      respuesta: [{value: '', disabled: true}],
      observaciones_coord: [{value: '', disabled: true}],
    });

  }

  loadData() {
    this.forma.controls.folio.patchValue(this.case.case_folio);
    this.forma.controls.no_control.patchValue(this.case.alum_id);
    this.forma.controls.nombre.patchValue(this.case.alum_name);
    this.forma.controls.correo.patchValue(this.case.alum_email);
    this.forma.controls.carrera.patchValue(this.case.coord_name);
    this.forma.controls.pregunta.patchValue(this.case.q_cont);
    this.forma.controls.observaciones.patchValue(this.case.q_obs);
    this.forma.controls.estado.patchValue(this.case.case_pro);
    this.forma.controls.respuesta.patchValue(this.case.a_cont);
    this.forma.controls.observaciones_coord.patchValue(this.case.a_obs);
  }

  generaAlerta() {
    // console.log(!!this.case.a_cont);

    if ( !this.case.a_cont ) {
      Swal.fire(
        'Caso en espera...',
        'Aun no hay respuesta de tu coordinador',
        'warning'
      );
    }
  }

  back() {
    this.location.back();
  }

}
