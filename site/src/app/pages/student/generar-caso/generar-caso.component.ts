import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, NgForm } from '@angular/forms';
import { CaseServiceService } from 'src/app/services/case-service.service';
import { Case } from 'src/app/models/Case';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DriverProvider } from 'protractor/built/driverProviders';
import CryptoJS from 'crypto-js';

@Component({
  selector: 'app-generar-caso',
  templateUrl: './generar-caso.component.html',
  styleUrls: ['./generar-caso.component.scss']
})
export class GenerarCasoComponent implements OnInit {

  header = 'Atención de Alumnos';

  forma: FormGroup;
  contador=0;
  preguntas: any = [];
  carreras: any = [];////              ARMANDO WAS HERE
  modHorario = false;
  materiasSemestre: any[] = [];
  case: Case;
  semestres = [1,2,3,4,5,6,7,8,9,10];

  obs_modifi = {
    semestre: '',
    materia: '',
    grupo: '',
  };

  constructor(
    private caseServ: CaseServiceService,
    private fb: FormBuilder,
    private router: Router,
  ) {
    this.crearFormulario();
  }

  ngOnInit() {
    this.caseServ.getQs()
    .subscribe(
      res => {
        this.preguntas = res;
        // console.log(this.preguntas);
        this.preguntas.unshift({
          q_cont: 'Caso de atención',
        });
        //console.log(this.forma);
      },
      err => console.log(err)
    );
    ///////////////
    this.caseServ.getCareers()
    .subscribe(
      res => {
        this.carreras = res;
        // console.log(this.carreras);
        this.carreras.unshift({
          career_name: 'Seleccione una carrera',//          ARMANDO WAS HERE
        });
        // console.log(this.forma);
      },
      err => console.log(err)
    );
    ////////////////////////
  }

  crearFormulario() {

    this.forma = this.fb.group({
      no_control: ['', [Validators.required, Validators.maxLength(20)] ],
      pass_student: ['', [Validators.required, Validators.maxLength(40)] ],
      nombre: ['', [Validators.required] ],
      // correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')] ],
      correo: ['', [Validators.required] ],
      carrera: ['', [Validators.required] ],
      pregunta: ['', [Validators.required] ],
      materias: this.fb.array([]),
      observaciones: ['', [Validators.required] ]
    });

  }

  public validaCampo( componente: string) {
    return this.forma.get(componente).invalid && this.forma.get(componente).touched;
  }

  get materias(): FormArray {
    return this.forma.get('materias') as FormArray;
  }

  validaModHorario() {
    if ( this.forma.get('pregunta').value === 'MODIFICACION DE HORARIO' || this.forma.get('pregunta').value === 'BAJA PARCIAL') {
      this.modHorario = true;
      this.contador++;
      this.agregarMateria();
    } else {
      this.modHorario = false;
      console.log(this.contador);
      for(this.contador;this.contador>=0;this.contador--){
        this.borrarMateria(this.contador);
      }
    }
  }

  agregarMateria() {
    this.materias.push( this.fb.group({
      semestre: [''],
      materia: [''],
      grupo: [''],
      alta_baja: [''],
    }));
    this.contador++;
  }

  borrarMateria( i: number ) {
    this.materias.removeAt(i);
    console.log("Borrado");
  }

  validaAltasBajas() {
    let sinEstado: boolean;
    this.forma.value.materias.forEach(mat => {
      if( !mat.alta_baja ) {
        // console.log('no hay alta/baja');
        sinEstado = true;
      }
    });
    if (sinEstado) {
      Swal.fire({
        icon: 'warning',
        title: 'Falta información',
        text: 'Comprueba que marcaste una de las casillas alta/baja en tus materias'
      });
      return true;
    }
  }

  // Obtiene las materias segun un indice dado, si el indice existe solo reemplaza las materias por el nuevo semesestre, si no existe el indice entonces agrega mas respuestas
  getMaterias(indx: number) {
    this.caseServ.getMaterias( this.forma.value.carrera, this.forma.value.materias[indx].semestre )
      .subscribe(
        res => {
          if ( !!this.materiasSemestre[indx] ) {
            this.materiasSemestre.splice(indx, 1, res);
          } else {
            this.materiasSemestre.push(res);
          }
        },
        err => {
          console.log(err);
        }
      );

  }


  getStudent() {
    const passphrase = "z&.GuFq)escM7'=+";
    const str_p = CryptoJS.AES.encrypt(this.forma.get('pass_student').value, passphrase).toString();

    var student_data = {
      student_id: this.forma.get('no_control').value,
      student_pass: str_p
    }
    // console.log(student_data)
    this.caseServ.getStudent( student_data )
      .subscribe(
        (res) => {
          this.forma.patchValue({
            nombre: res["alum_name"],
            correo: res["alum_email"]
          });
        },
        err => {
          //console.log(err)
          this.forma.patchValue({
            nombre: "No encontrado",
            correo: "No encontrado"
          });
        }
      );
  }

  guardar() {

    let materiasText: string = '';
    let observaciones: string = this.forma.get('observaciones').value;

    if( this.forma.invalid ) {

      return Object.values( this.forma.controls ).forEach( control => {
        // console.log(control);
        control.markAllAsTouched();
      });

    }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.forma.value.materias.forEach(mat => {
      // console.log(mat.materia);
      // tslint:disable-next-line: max-line-length
      materiasText += `\n\n<-------MATERIA------->\n- ${mat.alta_baja}\n-Semestre: ${mat.semestre}\n-Materia: ${mat.materia.mat_cont}\n-Grupo: ${mat.grupo}\n`
    });

    observaciones += materiasText;
    // console.log(observaciones);

    this.case = {
      case_folio: '',
      alum_id: this.forma.get('no_control').value,
      alum_name: this.forma.get('nombre').value.toUpperCase(),
      alum_email: this.forma.get('correo').value,
      coord_name: this.forma.get('carrera').value,
      q_cont: this.forma.get('pregunta').value,
      q_obs: observaciones
    };
    // console.log(this.case);

    if ( this.validaAltasBajas() ) { return };

    // console.log(this.forma);

    this.caseServ.newCase(this.case)
      .subscribe(
        (res) => {
          // console.log(res);
          let data: any;
          data = res
          Swal.fire({
            icon: 'success',
            title: 'Listo',
            text: `Te pedimos de la manera más atenta guardar este folio: ${data.Folio} ya que te servira para consultar tu caso en coordinación`
          });
          this.router.navigateByUrl('/');
        },
        (err) => {
          Swal.fire({
            icon: 'error',
            title: 'Error al generar tu caso',
            text: err.error.message
          });
        });
  }

}
