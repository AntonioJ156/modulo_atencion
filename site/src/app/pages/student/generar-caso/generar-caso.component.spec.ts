import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarCasoComponent } from './generar-caso.component';

describe('GenerarCasoComponent', () => {
  let component: GenerarCasoComponent;
  let fixture: ComponentFixture<GenerarCasoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerarCasoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarCasoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
