import { Routes } from '@angular/router';
import { ConsultaCasoComponent } from './consulta-caso/consulta-caso.component';
import { GenerarCasoComponent } from './generar-caso/generar-caso.component';


export const STUDENT_ROUTES: Routes = [
    { path: 'nuevo', component: GenerarCasoComponent },
    { path: 'consulta/:folio', component: ConsultaCasoComponent},
]