- [x] Boton regresar en editar preguntas y respuestas
- [x] Borrar coordinador
- [x] Error al no encontrar caso
- [x] Subir base de datos
- [x] Borrar todas las materias añadidas cuandos se cambie de situacion (revisar)
- [x] Etiqueta "Situacion"
- [x] Aparecer folio al generar el caso
- [x] Alerta al crear nuevo caso "Te pedimos de la manera más atenta guardar este folio ya que te servira para consultar tu caso en coordinación"
- [x] Error en la parte del coordinador en editar caso al agregar observaciones con estado "parcialmente".
- [x] Corregir texto en rojo al verificar número de control y el correo.
- [x] Corregir error con la respuesta al consultar un caso (no guarda la respuesta).

## Urgente
- [x] encriptar NIP
- [x] cambiar evento NIP
- [x] arreglar no agregar respuesta en editar caso "favor de seleccionar estado y respesta"
- [x] quitar fecha de restricción de generación de horario o poner alerta

## Antes del 2 - agosto
- [x] agregar mensaje al agregar coordinador
- [x] arreglar agregar coordinador
- [ ] fecha en que se realiza el caso
- [ ] fecha de atención (verde, amarillo, rojo)
