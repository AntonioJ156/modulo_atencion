-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 20-11-2020 a las 03:53:40
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `qrcoord`
--

-- --------------------------------------------------------

--
-- Creación del usuario 'modulo_admin' y
--

CREATE USER IF NOT EXISTS 'modulo_admin'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ZH<>&5AXkyymc)?*';
grant all privileges on qrcoord.* to 'modulo_admin'@'localhost';


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `alum_id` varchar(10) NOT NULL,
  `alum_nip` varchar(40) NOT NULL,
  `alum_name` varchar(100) NOT NULL,
  `alum_email` varchar(50) NOT NULL,
  `cord_id` int(2) NOT NULL,
  `alum_matdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer`
--

CREATE TABLE `answer` (
  `a_id` int(4) NOT NULL,
  `a_cont` varchar(1000) NOT NULL,
  `q_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `answer`
--

INSERT INTO `answer` (`a_id`, `a_cont`, `q_id`) VALUES
(101, 'ALTA DE ASIGNATURA', 1),
(102, 'BAJA DE ASIGNATURA', 1),
(103, 'AJUSTE DE HORARIO', 1),
(104, 'BAJA DE GRUPO/ASIGNATURA', 1),
(105, 'REGISTRO DE HORARIO', 1),
(106, 'CARGA DE HORARIO', 1),
(201, 'EN RIESGO POR CURSO(S) ESPECIAL(ES)', 2),
(202, 'ATRASADO CON MAS DE 2 SEMESTRES', 2),
(203, 'EN RIESGO POR MAS DE 3 CURSOS EN REPETICION', 2),
(204, 'NO CONCLUIR EL PLAN DE ESTUDIOS EN 12 SEMESTRES', 2),
(301, 'ALTA', 3),
(302, 'BAJA', 3),
(303, 'CONSULTA', 3),
(401, 'ASESORIA', 4),
(402, 'ENTREGA DE INFORME TECNICO', 4),
(403, 'JUSTIFICANTES', 4),
(501, 'JUSTIFICANTES', 5),
(601, 'SERVICIOS ESCOLARES', 6),
(602, 'VINCULACION', 6),
(603, 'FINANCIEROS', 6),
(604, 'D. ACADEMICA/JEFATURA', 6),
(605, 'D. ACADEMICOS', 6),
(606, 'BIBLIOTECA', 6),
(607, 'SUBDIRECCIONES', 6),
(608, 'C. COMPUTO', 6),
(609, 'LABORATORIOS', 6),
(701, 'INFORMACION', 7),
(702, 'SE VA', 7),
(703, 'VIENE', 7),
(801, 'INFORMACION', 8),
(901, 'TITULACION', 9),
(1001, 'COMITE ACADEMICO', 10),
(1101, 'OTROS', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `career`
--

CREATE TABLE `career` (
  `career_id` int(11) NOT NULL,
  `career_name` varchar(1000) NOT NULL,
  `career_sig` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `career`
--

INSERT INTO `career` (`career_id`, `career_name`, `career_sig`) VALUES
(0, 'Mecatrónica', 'MCT'),
(1, 'Arquitectura', 'ARQ'),
(2, 'Eléctrica', 'ELE'),
(3, 'Electrónica', 'ELO'),
(4, 'Industrial', 'IND'),
(5, 'Mecánica', 'MEC'),
(6, 'Sistemas Computacionales', 'ISC'),
(7, 'Gestión Empresarial', 'IGE'),
(8, 'Materiales', 'MAT'),
(9, 'Logística', 'LOG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `car_cord`
--

CREATE TABLE `car_cord` (
  `cc_id` int(10) NOT NULL,
  `cord_id` int(2) NOT NULL,
  `car_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `car_cord`
--

INSERT INTO `car_cord` (`cc_id`, `cord_id`, `car_id`) VALUES
(33, 2, 6),
(34, 1, 2),
(35, 1, 3),
(37, 3, 1),
(38, 4, 7),
(39, 5, 4),
(40, 6, 9),
(41, 7, 8),
(42, 8, 5),
(43, 9, 0),
(44, 99, 0),
(45, 99, 1),
(46, 99, 2),
(47, 99, 3),
(48, 99, 4),
(49, 99, 5),
(50, 99, 6),
(51, 99, 7),
(52, 99, 8),
(53, 99, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casos`
--

CREATE TABLE `casos` (
  `case_id` int(10) NOT NULL,
  `case_folio` varchar(20) NOT NULL,
  `alum_id` varchar(10) NOT NULL,
  `alum_name` varchar(100) NOT NULL,
  `alum_email` varchar(50) NOT NULL,
  `coord_name` varchar(20) NOT NULL,
  `cord_id` int(2) NOT NULL,
  `q_cont` varchar(1000) NOT NULL,
  `q_obs` varchar(2000) DEFAULT NULL,
  `a_cont` varchar(10000) DEFAULT NULL,
  `a_obs` varchar(2000) DEFAULT NULL,
  `case_pro` enum('PROCEDE','PARCIALMENTE','NO PROCEDE','N/A') DEFAULT 'N/A',
  `case_status` bit(1) NOT NULL DEFAULT b'0',
  `case_date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cord`
--

CREATE TABLE `cord` (
  `cord_id` int(2) NOT NULL,
  `cord_name` varchar(50) DEFAULT NULL,
  `cord_pass` varchar(300) DEFAULT NULL,
  `cord_email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cord`
--

INSERT INTO `cord` (`cord_id`, `cord_name`, `cord_pass`, `cord_email`) VALUES
(1, 'ELE - ELO', '$2b$10$3lmMHDWZbkw86uFFP9X5R.cuKosGnfqC.jxYFBcqUQZEPTKRLkuWK', 'dep_coordeye@queretaro.tecnm.mx'),
(2, 'Sistemas', '$2b$10$NOETLeAgcJJNsL8.C0gjP.NmdvNoP4OJ6CA4Rui.VX7g87oRd/YkG', 'dep_coordsyc@queretaro.tecnm.mx'),
(3, 'Arquitectura', '$2b$10$MnczTcMX1gXBHYdLdUmu2eY2P17jM7bgKC08i.gOn4aGDMzn9h0iq', 'dep_coordaqr@queretaro.tecnm.mx'),
(4, 'Gestion', '$2b$10$m4y5YDIw8DIoJFdzmeZ1s.cKP0H2wNJZ3z9041rGNjjzMVC11lVpa', 'dep_coordge@queretaro.tecnm.mx'),
(5, 'Industrial', '$2b$10$AdD2BdxYO51WRocM1qUQruzXW9YntIBe5JxFzqK40pGCSCqe28yyK', 'dep_coordind@queretaro.tecnm.mx'),
(6, 'Logistica', '$2b$10$M/jvfHpyldmnOGOVFIAFEO2CtzcyhU11h.4q2lzQxfIhOMstlSFzi', 'dep_coordlog@queretaro.tecnm.mx'),
(7, 'Materiales', '$2b$10$Qcr.Yi3HU1jM/W.bxJhrwuMURReCkqFMIWPLjrjdJY0nC6ivnrmsi', 'dep_coordmym@queretaro.tecnm.mx'),
(8, 'Mecanica', '$2b$10$JYZTvAn5a9ZKB.JdzWPG3exfZ..pGPATwtXBm/Z2VNCm9q0ylCf0W', 'dep_coordmym@queretaro.tecnm.mx'),
(9, 'Mecatronica', '$2b$10$f3t6VJ4x443C0bhU/CK3ouwFrbf2UucVmiu4iy8RNAV6GPQ.jAkJ.', 'dep_coordmct@queretaro.tecnm.mx'),
(99, 'Administrador', '$2b$10$bt9ryyO.l.Ag1TfKiDT4su7yv8MKtfL69u7RAsa36ke79SPg36hRW', 'dep@queretaro.tecnm.mx');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `mat_id` int(10) NOT NULL,
  `mat_cont` varchar(500) DEFAULT NULL,
  `car_id` int(2) DEFAULT NULL,
  `mat_sem` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

-- INSERT INTO `materia` (`mat_id`, `mat_cont`, `car_id`, `mat_sem`) VALUES
-- (1, 'Cálculo Diferencial', 6, 1),
-- (2, 'Fundamentos de Investigación', 6, 1),
-- (3, 'Fundamentos de Programación', 6, 1),
-- (4, 'Matemáticas Discretas', 6, 1),
-- (5, 'Taller de Administración', 6, 1),
-- (6, 'Taller de Etica', 6, 1),
-- (7, 'Tutoria 1', 6, 1),
-- (8, 'Cálculo Integral', 6, 2),
-- (9, 'Contabilidad Financiera', 6, 2),
-- (10, 'Principios Eléctricos y Aplicaciones Digitales', 6, 2),
-- (11, 'Programación Orientada a Objetos', 6, 2),
-- (12, 'Química', 6, 2),
-- (13, 'Tutoria 2', 6, 2),
-- (14, 'Álgebra Lineal', 6, 3),
-- (15, 'Arquitectura de Computadoras', 6, 3),
-- (16, 'Cálculo Vectorial', 6, 3),
-- (17, 'Estructuras de Datos', 6, 3),
-- (18, 'Física General', 6, 3),
-- (19, 'Probabilidad y Estadística', 6, 3),
-- (20, 'Ecuaciones Diferenciales', 6, 4),
-- (21, 'Investigación de Operaciones', 6, 4),
-- (22, 'Lenguajes de Interfaz', 6, 4),
-- (23, 'Lenguajes y Automatas I', 6, 4),
-- (24, 'Métodos Numéricos', 6, 4),
-- (25, 'Tópicos Avanzados de Programación', 6, 4),
-- (26, 'Fundamentos de Telecomunicaciones', 6, 5),
-- (27, 'Graficación', 6, 5),
-- (28, 'Lenguajes y Automatas II', 6, 5),
-- (29, 'Simulación', 6, 5),
-- (30, 'Sistemas Operativos', 6, 5),
-- (31, 'Sistemas Programables', 6, 5),
-- (32, 'Desarrollo Sustentable', 6, 6),
-- (33, 'Fundamentos de Bases de Datos', 6, 6),
-- (34, 'Programación Lógica y Funcional', 6, 6),
-- (35, 'Redes de Computadoras', 6, 6),
-- (36, 'Taller de Investigación I', 6, 6),
-- (37, 'Taller de Sistemas Operativos', 6, 6),
-- (38, 'Conmutación y Enrutamiento de Redes de Datos', 6, 6),
-- (39, 'Fundamentos de Ingeniería de Software', 6, 7),
-- (40, 'Inteligencia Artificial', 6, 7),
-- (41, 'Programación Distribuida', 6, 7),
-- (42, 'Taller de Bases de Datos', 6, 7),
-- (43, 'Taller de Investigación II', 6, 7),
-- (44, 'Administración de Bases de Datos', 6, 7),
-- (45, 'Administración de Redes', 6, 8),
-- (46, 'Cultura Empresarial', 6, 8),
-- (47, 'Ingeniería de Software', 6, 8),
-- (48, 'Ciencia de Datos', 6, 8),
-- (49, 'Programación WEB', 6, 8),
-- (50, 'Gestión de Proyectos de Software', 6, 8),
-- (51, 'Seguridad', 6, 9),
-- (52, 'Arquitectura Orientada a Servicios', 6, 9),
-- (53, 'Programación Móvil', 6, 9),
-- (54, 'Entornos Gráficos Interactivos (optativa)', 6, 9),
-- (55, 'Internet de las cosas (optativa)', 6, 9),
-- (56, 'Residencia Profesional', 6, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `query`
--

CREATE TABLE `query` (
  `q_id` int(2) NOT NULL,
  `q_cont` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `query`
--

INSERT INTO `query` (`q_id`, `q_cont`) VALUES
(1, 'MODIFICACION DE HORARIO'),
(2, 'ASESORIA DE AVANCE ACADEMICO'),
(3, 'ACTIVIDADES COMPLEMENTARIAS'),
(4, 'RESIDENCIA'),
(5, 'JUSTIFICANTES'),
(6, 'ASESORIA A PROCESOS ADMINISTRATIVOS'),
(7, 'MOVILIDAD, TRASLADOS, EQUIVALENCIA'),
(8, 'BAJA TEMPORAL Y DEFINITIVA'),
(9, 'TITULACION'),
(10, 'COMITE ACADEMICO'),
(11, 'OTROS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`alum_id`);

--
-- Indices de la tabla `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`a_id`);

--
-- Indices de la tabla `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`career_id`);

--
-- Indices de la tabla `car_cord`
--
ALTER TABLE `car_cord`
  ADD PRIMARY KEY (`cc_id`);

--
-- Indices de la tabla `casos`
--
ALTER TABLE `casos`
  ADD PRIMARY KEY (`case_id`);

--
-- Indices de la tabla `cord`
--
ALTER TABLE `cord`
  ADD PRIMARY KEY (`cord_id`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`mat_id`);

--
-- Indices de la tabla `query`
--
ALTER TABLE `query`
  ADD PRIMARY KEY (`q_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `car_cord`
--
ALTER TABLE `car_cord`
  MODIFY `cc_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `casos`
--
ALTER TABLE `casos`
  MODIFY `case_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `cord`
--
ALTER TABLE `cord`
  MODIFY `cord_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `mat_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
